
package paoexamen;

public class PaoExamen {

    class Nodo
      {
        int info;
        Nodo izq, der;
      }
      Nodo raiz;

public void insertar (int info)
      {
          Nodo nuevo;
          nuevo = new Nodo ();
          nuevo.info = info;
          nuevo.izq = null;
          nuevo.der = null;
          if (raiz == null)
              raiz = nuevo;
          else
          {
              Nodo anterior = null, reco;
              reco = raiz;
              while (reco != null)
              {
                  anterior = reco;
                  if (info < reco.info)
                      reco = reco.izq;
                  else
                      reco = reco.der;
              }
              if (info < anterior.info)
                  anterior.izq = nuevo;
              else
                  anterior.der = nuevo;
          }
      }   

      private void imprimirPre (Nodo reco)
      {
          if (reco != null)
          {
              System.out.print(reco.info + " ");
              imprimirPre (reco.izq);
              imprimirPre (reco.der);
          }
      }

      public void imprimirPre ()
      {
          imprimirPre (raiz);
          System.out.println();
      }

     

      public static void main (String [] ar)
      {
          PaoExamen abo = new PaoExamen ();
          abo.insertar (69);
          abo.insertar (82);
          abo.insertar (73);
          abo.insertar (75);
          abo.insertar (65);
          abo.insertar (80);
          abo.insertar (79);
          abo.insertar (76);
          abo.insertar (90);
          System.out.println ("Imprimir en preOrden: ");
          abo.imprimirPre ();
            
      }      
}
